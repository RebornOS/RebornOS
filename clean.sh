#  Copyright © 2016-2019 Reborn OS
#
#  This file is part of Reborn OS.
#
#  Reborn OS is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Reborn OS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  The following additional terms are in effect as per Section 7 of the license:
#
#  The preservation of all legal notices and author attributions in
#  the material or in the Appropriate Legal Notices displayed
#  by works containing it is required.
#
#  You should have received a copy of the GNU General Public License
#  along with Reborn OS; If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo
echo "Removing work directory!"
rm -rf work
echo "Removed!"

echo
echo "Removing out directory!"
rm -rf out
echo "Removed!"

echo
echo "Cleaning pacman caches before new build!"
pacman -Scc --noconfirm --quiet
echo "Cleaned!"

echo
echo "Ensuring package cache is truly clean!"
rm -rf /var/cache/pacman/pkg/*
echo "All gone!"

echo
echo "Resyncing databases before new build!"
pacman -Syy --quiet
echo "Sync'd!"

echo
echo "All done! You are now ready to build!"
echo



